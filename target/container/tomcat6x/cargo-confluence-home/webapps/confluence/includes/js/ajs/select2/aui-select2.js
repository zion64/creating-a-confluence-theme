/**
 * Wraps a vanilla Select2 with ADG _style_, as an auiSelect2 method on jQuery objects.
 *
 * @since 5.1
 */
(function($) {

    /**
     * We make a copy of the original select2 so that later we might re-specify $.fn.auiSelect2 as $.fn.select2. That
     * way, calling code will be able to call $thing.select2() as if they were calling the original library,
     * and ADG styling will just magically happen.
     *
     * This JS code will always be necessary while the CSS overrides rely on aui-select2-container, aui-select2-drop and
     * aui-button classes.
     */
    var originalSelect2 = $.fn.select2;

    $.fn.auiSelect2 = function () {
        var select2 = originalSelect2.apply(this, arguments);

        // CONFDEV-12634: grant ADG look and feel to select2
        // 'aui-select2-container' is on the top-level element for the select2 in the DIALOG
        // 'aui-select2-drop' is on the top-level element for the select2 dropdown in the BODY
        // 'aui-select2-choice' is in the container and displays the currently chosen option
        var parent = this.parent();
        parent.find(".select2-container").addClass("aui-select2-container").removeClass('select2-container');
        parent.find(".select2-drop").addClass("aui-select2-drop aui-dropdown2 aui-style-default").removeClass('select2-drop');
        parent.find(".select2-choice").addClass("aui-select2-choice aui-button").removeClass('select2-choice');
    };

    /**
     * Extra delegate on top of the one in Select2 - we need to find the aui-classed elements.
     * This is probably the worst code in all of these ADG overrides, and the most fragile.
     */
    $(document).ready(function () {
        $(document).delegate("body", "mousedown touchend", function (e) {
            var target = $(e.target).closest("div.aui-select2-container").get(0), attr;
            if (target) {
                $(document).find("div.select2-container-active").each(function () {
                    if (this !== target) $(this).data("select2").blur();
                });
            } else {
                target = $(e.target).closest("div.aui-select2-drop").get(0);
                $(document).find("div.select2-drop-active").each(function () {
                    if (this !== target) $(this).data("select2").blur();
                });
            }

            target=$(e.target);
            attr = target.attr("for");
            if ("LABEL" === e.target.tagName && attr && attr.length > 0) {
                target = $("#"+attr);
                target = target.data("select2");
                if (target !== undefined) { target.focus(); e.preventDefault();}
            }
        });
    });

})(jQuery);