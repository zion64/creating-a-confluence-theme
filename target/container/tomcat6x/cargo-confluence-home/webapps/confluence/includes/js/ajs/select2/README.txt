This directory contains 2 kinds of files:

# original Select2 source, including select2.js, select2.css and the LICENCE text file
# overrides for AUI Select2

Rule 1: don't change the first kind. This will make it harder to move the select2
code into AUI from Confluence and apply Select2 updates.

Rule 2: write overrides in such a way that they won't break any vanilla usages
of select2 in the product (for example, in Team Calendars)

When select2 makes its way into AUI (see AUI-1301) the select2 files can be removed from
this directory. In a perfect world, our overrides will be used in the AUI version in a way
that they can be removed from here also.