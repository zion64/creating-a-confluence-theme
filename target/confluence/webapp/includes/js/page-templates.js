/**
 * Used by createpage-entervariables.vm to update Template Variable form fields embedded in the rendered wiki content.
 */
AJS.toInit(function($) {

    function syncInputs() {
        // The same template variable may appear multiple times in the template. Keep the values synchronized.
        var that = this;
        $('input[name="' + that.name + '"]').not(that).each(function (index, field) {
            field.value = that.value;
        });
    }

    var fillTemplateForm = $('form[name="filltemplateform"]');
    var templateTextFields = $('.wiki-content input.page-template-field');

    templateTextFields.keyup(function(e) {
        syncInputs.call(this);

        // User should still be able to submit the "create-from-template" form with enter key.
        if (e.which == 13) { //Enter
            fillTemplateForm.submit();
        }
    });
    templateTextFields.change(syncInputs);  // field may change without typing (e.g. paste)

    // On form submit, copy all of the page-template form fields from the wiki-content area to the form we are
    // submitting.
    fillTemplateForm.submit(function() {
        $('.wiki-content .page-template-field[name^="variableValues."]').clone().hide().appendTo(this);
        AJS.debug("Page template fields moved to submission form");
    });

    // Kill all form-submission inside the wiki-content - give other form-JS time to do bindings, in case this logic
    // runs *before* the bindings are added.
    setTimeout(function() {
        $('.wiki-content form').unbind().submit(function(e) {
            // Really try to kill the event...
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            return false;
        });
    }, 100);

    // Disabling any button that may be rendered as part of the template content
    $('.view-template .aui-button').attr('aria-disabled', 'true');
});
